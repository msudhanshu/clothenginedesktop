package it.polito.teaching.cv;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import clothengineopencv.sg.clothEngine.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
//import org.opencv.videoio.VideoCapture;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * The controller associated with the only view of our application. The
 * application logic is implemented here. It handles the button for
 * starting/stopping the camera, the acquired video stream, the relative
 * controls and the image segmentation process.
 * 
 * @author <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * @version 1.5 (2015-11-24)
 * @since 1.0 (2013-12-20)
 * 		
 */
public class ImageSegController implements IShowImageInUI,Ilogger
{
	
	// FXML buttons
	@FXML
	private Button cameraButton;
	// the FXML area for showing the current frame
	@FXML
	private ImageView originalFrame;
	// checkbox for enabling/disabling Canny
	@FXML
	private CheckBox canny;
	// canny threshold value
	@FXML
	private Slider threshold;
	// checkbox for enabling/disabling background removal
	@FXML
	private CheckBox dilateErode;
	// inverse the threshold value for background removal
	@FXML
	private CheckBox inverse;
	
	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	// the OpenCV object that performs the video capture
	//private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive;


	private String imageName = "ts-6.png";

	private boolean useCamera = false;

	/**
	 * The action triggered by pushing the button on the GUI
	 */
	@FXML
	protected void staticImage() {
		// set a fixed width for the frame
		originalFrame.setFitWidth(380);
		// preserve image ratio
		originalFrame.setPreserveRatio(true);

		originalFrame.setStyle("-fx-background-color: DAE6F3;");
		ClothVision clothVision = new ClothVision(); //imageName
		clothVision.setImageUIViewer(this);
		clothVision.setLogger(this);
		clothVision.setPlatformUtil(new DesktopPlatformUtil());
		Mat m = ClothVision.platformUtil.readImage(null, imageName, -1,true);

		//CELogImage.showInUI(m);
		ClothImage clothImage = new ClothImage(m);
		clothVision.processCloth(clothImage);
		System.out.println("Color = " + clothVision.getMainColor());
		clothVision.generateUnwrapTexture();

		String unwrappedTextureFile= "/Users/manjeet/UnityLab/VirtualMe/UnitycustomTshirtSite/customchar2/Assets/Content/Model/FuseModel-p13196@T-Pose 1.fbm/FuseModel-p13196_Top_diffuse.png";
		//clothVision.writeUnwrappedTexture(unwrappedTextureFile);
		Imgcodecs.imwrite(unwrappedTextureFile,clothVision.getFinalUnwrappedTexture());

		Mat frame = clothVision.getFullClothMat();

		Mat processedMat = processImage(frame);
		//processedMat.setTo()
//		Mat Mask = new Mat();
//		Mask = processedMat.colRange(0,200);
//		processedMat.setTo(processedMat,Mask);
//
//		for (int i=0; i< processedMat.size().height; i++){
//			for (int j = 0; j<processedMat.size().width; j++){
//				//if (processedMat.get(i,j)[3]==0)
//					processedMat.get(i,j)[1] = processedMat.get(i,j)[3] = 1;
//			}
//		}
//		copyMakeBorder()
//
//				new Mat()
//
//		// make the border with a size of 10px for each side
//		Imgproc.copyMakeBorder(processedMat, processedMat, 10, 10,
//				10, 10, Imgproc., new Scalar(255,0,0));

		Image imageToShow = mat2Image(processedMat);
		originalFrame.setImage(imageToShow);
	}

		/**
         * The action triggered by pushing the button on the GUI
         */
	@FXML
	protected void startCamera()
	{
		// set a fixed width for the frame
		originalFrame.setFitWidth(380);
		// preserve image ratio
		originalFrame.setPreserveRatio(true);
		
		if (!this.cameraActive)
		{
			// disable setting checkboxes
			this.canny.setDisable(true);
			this.dilateErode.setDisable(true);
			
			// start the video capture
			//temp cvvideooff///this.capture.open(0);
			
			// is the video stream available?
			//temp cvvideooff///if (this.capture.isOpened())
			if(true)
			{
				this.cameraActive = true;
				
				// grab a frame every 33 ms (30 frames/sec)
				Runnable frameGrabber = new Runnable() {
					
					@Override
					public void run()
					{
						Image imageToShow = grabFrame();
						originalFrame.setImage(imageToShow);
					}
				};
				
				this.timer = Executors.newSingleThreadScheduledExecutor();
				this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
				
				// update the button content
				this.cameraButton.setText("Stop Camera");
			}
			else
			{
				// log the error
				System.err.println("Failed to open the camera connection...");
			}
		}
		else
		{
			// the camera is not active at this point
			this.cameraActive = false;
			// update again the button content
			this.cameraButton.setText("Start Camera");
			// enable setting checkboxes
			this.canny.setDisable(false);
			this.dilateErode.setDisable(false);
			// stop the timer
			try
			{
				this.timer.shutdown();
				this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException e)
			{
				// log the exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
			
			// release the camera
			//temp cvvideooff///this.capture.release();
			// clean the frame
			this.originalFrame.setImage(null);
		}
	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 * @return the {@link Image} to show
	 */
	private Image grabFrame()
	{
		// init everything
		Image imageToShow = null;
		Mat frame = new Mat();
		// check if the capture is open
		//temp cvvideooff///	if (this.capture.isOpened())
		{
			try
			{
				// read the current frame
				//temp cvvideooff///this.capture.read(frame);
				// convert the Mat object (OpenCV) to Image (JavaFX)
				imageToShow = mat2Image(processImage(frame));
				
			}
			catch (Exception e)
			{
				// log the (full) error
				System.err.print("ERROR");
				e.printStackTrace();
			}
		}
		return imageToShow;
	}


	private Mat processImage(Mat frame) {
		// if the frame is not empty, process it
		if (!frame.empty())
		{
			// handle edge detection
			if (this.canny.isSelected())
			{
				frame = this.doCanny(frame);
			}
			// foreground detection
			else if (this.dilateErode.isSelected())
			{
				frame = this.doBackgroundRemoval(frame);
			}
			return frame;
		}
		return frame;
	}

	/**
	 * Perform the operations needed for removing a uniform background
	 *
	 * @param frame
	 *            the current frame
	 * @return an image with only foreground objects
	 */
	private Mat doBackgroundRemoval(Mat frame)
	{
		// init
		Mat hsvImg = new Mat();
		List<Mat> hsvPlanes = new ArrayList<>();
		Mat thresholdImg = new Mat();

		int thresh_type = Imgproc.THRESH_BINARY_INV;
		if (this.inverse.isSelected())
			thresh_type = Imgproc.THRESH_BINARY;

		// threshold the image with the average hue value
		hsvImg.create(frame.size(), CvType.CV_8U);
		Imgproc.cvtColor(frame, hsvImg, Imgproc.COLOR_BGR2HSV);
		Core.split(hsvImg, hsvPlanes);

		// get the average hue value of the image
		double threshValue = this.getHistAverage(hsvImg, hsvPlanes.get(0));

		Imgproc.threshold(hsvPlanes.get(0), thresholdImg, threshValue, 179.0, thresh_type);

		Imgproc.blur(thresholdImg, thresholdImg, new Size(5, 5));

		// dilate to fill gaps, erode to smooth edges
		Imgproc.dilate(thresholdImg, thresholdImg, new Mat(), new Point(-1, -1), 1);
		Imgproc.erode(thresholdImg, thresholdImg, new Mat(), new Point(-1, -1), 3);

		Imgproc.threshold(thresholdImg, thresholdImg, threshValue, 179.0, Imgproc.THRESH_BINARY);

		// create the new image
		Mat foreground = new Mat(frame.size(), CvType.CV_8UC3, new Scalar(255, 255, 255));
		frame.copyTo(foreground, thresholdImg);


		Mat threshold_output = new Mat(foreground.size(),CvType.CV_8UC1);
		/// Detect edges using Threshold
		Imgproc.threshold(foreground, threshold_output, 20, 255, Imgproc.THRESH_BINARY);
		Imgproc.cvtColor(threshold_output,threshold_output,CvType.CV_8UC1);

		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(threshold_output, contours, hierarchy, Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_SIMPLE);
		for (int i = 0; i < contours.size(); i++) {
			double[] c = hierarchy.get(0, i);
			Rect rect = Imgproc.boundingRect(contours.get(i));
			Imgproc.rectangle(foreground, new Point(rect.x, rect.y),
					new Point(rect.x + rect.width, rect.y + rect.height),
					new Scalar(0, 255, 0), 3);
		}
		return foreground;
	}

	/**
	 * Get the average hue value of the image starting from its Hue channel
	 * histogram
	 *
	 * @param hsvImg
	 *            the current frame in HSV
	 * @param hueValues
	 *            the Hue component of the current frame
	 * @return the average Hue value
	 */
	private double getHistAverage(Mat hsvImg, Mat hueValues)
	{
		// init
		double average = 0.0;
		Mat hist_hue = new Mat();
		// 0-180: range of Hue values
		MatOfInt histSize = new MatOfInt(180);
		List<Mat> hue = new ArrayList<>();
		hue.add(hueValues);

		// compute the histogram
		Imgproc.calcHist(hue, new MatOfInt(0), new Mat(), hist_hue, histSize, new MatOfFloat(0, 179));

		// get the average Hue value of the image
		// (sum(bin(h)*h))/(image-height*image-width)
		// -----------------
		// equivalent to get the hue of each pixel in the image, add them, and
		// divide for the image size (height and width)
		for (int h = 0; h < 180; h++)
		{
			// for each bin, get its value and multiply it for the corresponding
			// hue
			average += (hist_hue.get(h, 0)[0] * h);
		}

		// return the average hue of the image
		return average = average / hsvImg.size().height / hsvImg.size().width;
	}

	/**
	 * Apply Canny
	 *
	 * @param frame
	 *            the current frame
	 * @return an image elaborated with Canny
	 */
	private Mat doCanny(Mat frame)
	{
		// init
		Mat grayImage = new Mat();
		Mat detectedEdges = new Mat();

		// convert to grayscale
		Imgproc.cvtColor(frame, grayImage, Imgproc.COLOR_BGR2GRAY);

		// reduce noise with a 3x3 kernel
		Imgproc.blur(grayImage, detectedEdges, new Size(3, 3));

		// canny detector, with ratio of lower:upper threshold of 3:1
		Imgproc.Canny(detectedEdges, detectedEdges, this.threshold.getValue(), this.threshold.getValue() * 3);

		// using Canny's output as a mask, display the result
		Mat dest = new Mat();
		frame.copyTo(dest, detectedEdges);

		return dest;
	}
	
	/**
	 * Action triggered when the Canny checkbox is selected
	 * 
	 */
	@FXML
	protected void cannySelected()
	{
		// check whether the other checkbox is selected and deselect it
		if (this.dilateErode.isSelected())
		{
			this.dilateErode.setSelected(false);
			this.inverse.setDisable(true);
		}
		
		// enable the threshold slider
		if (this.canny.isSelected())
			this.threshold.setDisable(false);
		else
			this.threshold.setDisable(true);
			
		// now the capture can start
		this.cameraButton.setDisable(false);
	}
	
	/**
	 * Action triggered when the "background removal" checkbox is selected
	 */
	@FXML
	protected void dilateErodeSelected()
	{
		// check whether the canny checkbox is selected, deselect it and disable
		// its slider
		if (this.canny.isSelected())
		{
			this.canny.setSelected(false);
			this.threshold.setDisable(true);
		}
		
		if (this.dilateErode.isSelected())
			this.inverse.setDisable(false);
		else
			this.inverse.setDisable(true);
			
		// now the capture can start
		this.cameraButton.setDisable(false);
	}
	
	/**
	 * Convert a Mat object (OpenCV) in the corresponding Image for JavaFX
	 * 
	 * @param frame
	 *            the {@link Mat} representing the current frame
	 * @return the {@link Image} to show
	 */
	private Image mat2Image(Mat frame)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer, according to the PNG format
		Imgcodecs.imencode(".png", frame, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	@Override
	public boolean addMatInUIStack(Mat mat) {
		return addMatInUIStack(mat,"Processed Image");
	}

	@Override
	public boolean addMatInUIStack(Mat mat,String title) {
//		Mat mask = new Mat();
//		Imgproc.cvtColor(mat, mask, Imgproc.COLOR_BGRA2GRAY);
	//	mask =	mask.colRange(150,200);
		//mat.setTo(mat,mask);

//		for (int i=0; i< mat.size().height; i++){
//			for (int j = 0; j<mat.size().width; j++) {
//				double[] data = mat.get(i, j);
//				if (data.length == 4)
//				{
//					if (data[3] <= 0) {
//						data[0] = 0;
//						data[1] = 255;
//						data[2] = 0;
//						data[3] = 100;
//						//mat.put(i, j, data);
//					}
//				}
//			}
//		}


		Image imageToShow = mat2Image(mat);
		Stage stage = new Stage();
		try
		{
			// load the FXML resource
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("cvimageuiview.fxml"));
			// set a whitesmoke background
			root.setStyle("-fx-background-color: whitesmoke; -fx-background-color: DAE6F3; ");
			// create and style a scene
			Scene scene = new Scene(root, 500, 500);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			// create the stage with the given title and the previously created
			// scene
			stage.setTitle(title);
			stage.setScene(scene);
			// show the GUI
			stage.show();
			ImageView frame = new ImageView(imageToShow);
			frame.setFitWidth(400);
			// preserve image ratio
			frame.setPreserveRatio(true);
			frame.setStyle("-fx-background-color: AAE1F3;");
			root.getChildren().add(frame);


		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public void log(String tag, String msg) {
		System.out.println(tag+":"+msg);
	}

	@Override
	public void logError(String tag, String msg) {
		System.out.println(tag+":"+msg);
	}
}
