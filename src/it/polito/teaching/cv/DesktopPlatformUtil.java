package it.polito.teaching.cv;

import clothengineopencv.sg.clothEngine.CELog;
import clothengineopencv.sg.clothEngine.IPlatformUtil;

import java.awt.*;

/**
 * Created by manjeet on 27/05/16.
 */
public class DesktopPlatformUtil extends IPlatformUtil {
    @Override
    public String getStoragePath() {
        String OPENCV_JAVA_CLOTH_PATH = "OPENCV_JAVA_CLOTH_PATH";
        String projectPath = System.getenv().get(OPENCV_JAVA_CLOTH_PATH);
        if(projectPath == null || projectPath.isEmpty()) {
            CELog.e("set env variable for RESOURCES path : "+OPENCV_JAVA_CLOTH_PATH);
        }
        return projectPath + "/res";
    }

    @Override
    public void RGBtoHSB(int r, int g, int b, float[] hsv) {
        Color.RGBtoHSB(r, g, b, hsv);
    }
}
