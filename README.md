
Intellij Setting:
1.In open module setting->Modules->add jar = opencv-310.jar.
/Users/manjeet.s/Development/SDK/OpenCV/opencv-3.1.0/build/bin/opencv-310.jar

2.In Run Configuration:
-Djava.library.path=/Users/manjeet.s/Development/SDK/OpenCV/opencv-3.1.0/build/lib


3.ln -s libopencv_java300.so libopencv_java300.dylib
(if dylib has not bin created)

4.export OPENCV_JAVA_CLOTH_PATH=/Users/manjeet/UnityLab/VirtualMe/opencv/intelijworkspace/OpencvJavaCloth






## Image Segmentation with OpenCV and JavaFX

*Computer Vision course - [Politecnico di Torino](http://www.polito.it)*

A project, made in Eclipse (Mars), for experimenting with edge detection, erosion and dilatation. It performs image segmentation upon a webcam video stream. Some screenshots of the running project are available in the `results` folder.

Please, note that the project is an Eclipse project, made for teaching purposes. Before using it, you need to install the OpenCV library (version 3.0) and JavaFX (version 2 or superior) and create a `User Library` named `opencv2` that links to the OpenCV jar and native libraries.

A guide for getting started with OpenCV and Java is available at [http://opencv-java-tutorials.readthedocs.org/en/latest/index.html](http://opencv-java-tutorials.readthedocs.org/en/latest/index.html).



CV Concepts:

1. ///// Find contours,  Approximate contours to polygons + get bounding rects
//http://answers.opencv.org/question/60149/crop-image-after-border-detection/
 Mat img = imread("crop_rectangle.png", CV_LOAD_IMAGE_GRAYSCALE);

    Mat threshold_output;
    /// Detect edges using Threshold
    threshold( img, threshold_output, 20, 255, THRESH_BINARY );

    imshow( "threshold_output", threshold_output );

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    /// Find contours
    findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    /// Approximate contours to polygons + get bounding rects
    vector<vector<Point> > contours_poly( contours.size() );
    Rect boundRect;

    double maxArea = 0.0;
    for( int i = 0; i < contours.size(); i++ )
    {
        double area = contourArea(contours[i]);
        if(area > maxArea) {
            maxArea = area;
            approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
            boundRect = boundingRect( Mat(contours_poly[i]) );
        }
    }



2. copyTo:
http://stackoverflow.com/questions/22428448/copying-a-small-image-into-the-camera-frame-with-opencv-for-android
// (3) Resizing and inserting an arbitrary grey image into the rgba camera frame
Imgproc.resize(mComparatorImage, mResizedImageClone, new Size (200, 100));
Imgproc.cvtColor(mResizedImageClone, mResizedImageClone, Imgproc.COLOR_GRAY2RGBA);
Mat submat = mOutputFrame.submat(0, 100, 0, 200);
mResizedImageClone.copyTo(submat);

3. lookuptable

4.floodfill

5. kmean clustering

6. kalman

7. Mistery of Mat : http://docs.opencv.org/2.4/doc/tutorials/core/mat_the_basic_image_container/mat_the_basic_image_container.html#matthebasicimagecontainer
Copy just header and refrence to data, not the whole data matric. assignment operator and the copy constructor only copies the header
Mat B(A);  //copy constructor
ROI: Mat D (A, Rect(10, 10, 100, 100) ); // using a rectangle
Mat E = A(Range::all(), Range(1,3)); // using row and column boundaries ????

clone() and copyTo() copies the data as well.


/////////////////////////////////////////////////////////////
/////////////TODO/////////////////
 //TODO : TEMP : THERE SHOULD BE A INTERFACE/ABSTRACTION FOR FILE, IT CAN BE SDCARD,LOCAL,SERVER ETC.. AND READING/ACCESSING THIS FILE VARIES UPON PLATFORM AS WELL

UMA: It allows developers to swap out mesh parts (like feet) and textures to create unique characters.

   ////////////////////////////////////////////////
      //TODO: add logo with alpha overlay, transucency , alpha blending implement
       //TODO : BETTER WAY IS TO TAKE MEAN OF CLUSTER HASMAP /// get main color . util.java
